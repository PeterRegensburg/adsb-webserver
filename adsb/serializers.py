# https://youtu.be/cJveiktaOSQ?feature=shared
from rest_framework import serializers
from .models import ADS_B
import csv
from io import StringIO


class ADS_BSerializer(serializers.ModelSerializer):
    class Meta:
        model = ADS_B
        fields = "__all__"


def ADS_BSerializer_to_csv(request):
    data = ADS_B.objects.all()
    serializer = ADS_BSerializer(data, many=True)

    output = StringIO(newline="")
    csv_writer = csv.writer(output)

    csv_writer.writerow(serializer.data[0].keys())  # Write header row

    for item in serializer.data:
        csv_writer.writerow(item.values())  # Write data rows

    csv_string = output.getvalue()
    output.close()

    return csv_string
