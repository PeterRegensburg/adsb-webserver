from django.db import models


class ADS_B(models.Model):
    # Unix timestamp with milliseconds. Example 1678646308.828
    timestamp = models.FloatField(null=False, help_text="Timestamp of the data")

    # Example 3c61f8
    identifier = models.CharField(
        max_length=6,
        unique=True,
        null=False,
        default="01ABCD",
        help_text="ICAO address of the aircraft",
    )

    # Example 51.765015
    lat = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=False,
        default=0,
        help_text="Aircraft latitude",
    )

    # Example 5.035004
    lon = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=False,
        default=0,
        help_text="Aircraft longitude",
    )

    # Example 212
    # cog = models.DecimalField(max_digits=5, decimal_places=1, null=True, default = 0)
    cog = models.CharField(max_length=6, default="")

    # Example 424
    # sog = models.DecimalField(max_digits=5, decimal_places=1, null=True, default = 0,help_text="Aircraft ground speed (in knots)")
    sog = models.CharField(
        max_length=6, default="", help_text="Aircraft ground speed (in knots)"
    )

    # Example 40000
    # alt = models.DecimalField(max_digits=6, decimal_places=1, null=True, default = 0,help_text="Aircraft altitude (in feet)")
    alt = models.CharField(
        max_length=6, default="", help_text="Aircraft altitude (in feet)"
    )
