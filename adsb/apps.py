from django.apps import AppConfig


class AdsbConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'adsb'
