import datetime
import socket
import threading

from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.decorators import authentication_classes, permission_classes

from .helpers import is_jwt_token_expired
import csv

from ais.models import AIS
from adsb.models import ADS_B

from ais.serializers import AIS_Serializer_to_csv, AIS_Serializer
from adsb.serializers import ADS_BSerializer, ADS_BSerializer_to_csv
from datetime import datetime


database = ""


def update_database(received_data):
    global database

    if database == "ais":
        DB = AIS
    else:
        DB = ADS_B

    csv_data = received_data.decode("utf-8")
    csv_data = csv_data.replace("\r\n", "\n")
    data = []

    csv_reader = csv.reader(csv_data.splitlines())

    for row in csv_reader:
        data.append(row)

    field_mapping = {field: index for index, field in enumerate(data[0])}

    print(field_mapping)
    data_list = []

    for row in data[1:]:
        row_dict = {field: row[field_mapping[field]] for field in field_mapping}
        data_list.append(row_dict)

    updated = 0
    created = 0
    ignored = 0

    for data in data_list:
        timestamp = data.get("timestamp")
        identifier = data.get("identifier")

        lat = data.get("lat")
        lon = data.get("lon")
        cog = data.get("cog", "0")
        sog = data.get("sog", "0")

        if cog == "":
            cog = "0"
        if sog == "":
            sog = "0"
        if database == "ADSB":
            alt = data.get("alt", "0")
            if alt == "":
                alt = "0"

        if identifier is not None and timestamp is not None:
            try:
                db_object = DB.objects.get(identifier=identifier)

                try:
                    ts_float = float(timestamp)
                    try:
                        ts1 = datetime.utcfromtimestamp(ts_float)

                        ts2 = datetime.utcfromtimestamp(db_object.timestamp)

                    except Exception as e:
                        # Send a response to the client
                        response = f"Error. {e}"
                        return response

                    if ts1 > ts2:
                        db_object.timestamp = timestamp
                        db_object.lat = lat
                        db_object.lon = lon
                        db_object.cog = cog
                        db_object.sog = sog
                        db_object.save()
                        updated += 1
                    else:
                        ignored += 1

                except Exception as e:
                    return f"AIS ERRORR. {e}"

            except DB.DoesNotExist:
                if database == "AIS":
                    new_entry = AIS(
                        identifier=identifier,
                        timestamp=timestamp,
                        lat=lat,
                        lon=lon,
                        cog=cog,
                        sog=sog,
                    )

                else:
                    new_entry = ADS_B(
                        identifier=identifier,
                        timestamp=timestamp,
                        lat=lat,
                        lon=lon,
                        cog=cog,
                        sog=sog,
                        alt=alt,
                    )

                try:
                    new_entry.save()
                except Exception as e:
                    return f"An error occurred saving new_entry: {str(e)}"
                    # return Response(
                    #    {
                    #        "ERRORmessage": f"An error occurred saving new_entry: {str(e)}"
                    #    },
                    #    status=500,
                    # )

                created += 1

    return "DONE"


# Function to handle a single client connection
def handle_client(client_socket):
    while True:
        data = client_socket.recv(1024)
        if not data:
            break  # No more data from the client

        # Process the received data using your custom function
        update_database(data.decode())

    client_socket.close()


def start_socket_server(request):
    global database
    if request.path.find("ais") != -1:
        database = "AIS"
    else:
        database = "ADSB"

    host = "0.0.0.0"  # Listen on all available network interfaces
    port = 12345
    try:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((host, port))
        server_socket.listen(5)
    except Exception as e:
        return False
    print(f"Socket server is listening on {host}:{port}")
