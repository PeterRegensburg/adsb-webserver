from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from .socket_server import start_socket_server

from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.response import Response


@api_view(["GET"])
# @authentication_classes([JWTAuthentication])
# @permission_classes([IsAuthenticated])
def start_socket_view(request):
    result = start_socket_server(request)
    return Response({"message": f"Started"}, status=200)
