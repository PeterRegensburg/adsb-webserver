from django.contrib import admin
from rest_framework.response import Response
from django.urls import path, include
from .views import start_socket_view

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("ais/", include("ais.urls")),
    path("adsb/", include("adsb.urls")),
    path("token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("token/verify/", TokenVerifyView.as_view(), name="token_verify"),
    path("start_socketais/", start_socket_view),
    path("start_socketadsb/", start_socket_view),
]
