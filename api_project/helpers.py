import jwt
import time


def is_jwt_token_expired(token):
    """Check if access_token is valid.
    Return True if expired
    """
    try:
        payload = jwt.decode(token, verify=False)
        expiration_timestamp = payload["exp"]
        current_time = int(time.time())
        return current_time > expiration_timestamp
    except jwt.ExpiredSignatureError:
        return True  # Token has already expired
    except jwt.InvalidTokenError:
        return True  # Token is invalid
