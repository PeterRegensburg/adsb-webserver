
# INTERNAL DJANGO REST framework

## Tutorial Django REST Framework Oversimplified

<https://youtu.be/cJveiktaOSQ?feature=shared>

## Documentation Django

<https://www.django-rest-framework.org/>

## Authentication & Authorization

<https://youtu.be/7Q17ubqLfaM?feature=shared>

## Authentication

Username / Password

## Authorization

**Session  ID**

- POST email/password
- Server stores and sends session ID
- Client stores session ID as Cookie. Sends session ID with next request

**JWT**

- POST email/password
- Server creates JWT and encodes and signs it  with servers secret key. With userinfo ***NO storage***  
- Server sends JWT
- Client sends JWT with next request
- Server verifies. Is token signed with servers secret key?
- Server deserializes JWT and has userinfo from JWT

**jwt.io**  
<https://jwt.io/>

## Debugger setup in VS Code

Python:Select Interpreter
Path to venv/Scripts/python.exe

## python manage.py runserver 0.0.0.0:8000
in launch.json

"args": [  
            	"runserver",  
            	"0.0.0.0:8000"
        	],

# Service adsb_server.service

```sudo nano /etc/systemd/system/adsb_server.service```

```
[Unit]
Description=ADSB Webserver
After=network.target

[Service]
User=pi
Group=pi
WorkingDirectory=/home/pi/vessel_flight_tracker
ExecStart=/home/pi/vessel_flight_tracker/venv/bin/python /home/pi/vessel_flight>
[Install]
WantedBy=multi-user.target

```

```sudo systemctl start adsb_server.service  ```

```sudo systemctl enable adsb_server.service  ```

# Install new system


pip install -r requirements.txt

python manage.py migrate

python manage.py createsuperuser  pedi pedi










