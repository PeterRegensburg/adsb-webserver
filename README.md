# VFT vessel_flight_tracker and AIS_tracker API 
VFT API aircraft position data in a database ADSB. The most current position is saved for each aircraft identifier.

Structure ADSB database
- timestamp
- identifier
- lat
- lon
- cog
- sog
- alt

Structure AIS database identical. Only no alt
- timestamp
- identifier
- lat
- lon
- cog
- sog

# API 
## /adsb/get/  
## /ais/get/
return csv with all entries in database ADS-B or AIS

Example:
<pre>timestamp,identifier,lat,lon,cog,sog,alt
1678646309.828,3c61f8,51.765015,5.035004,212,424,40000
1678646308.828,3c5ee2,51.209839,5.413216,,,0
1678646308.828,3c5ef2,51.209839,5.413216,,,0</pre>

## /adsb/add/    or /ais/add/
Adds data to database if 
- identifier new or
- identifier is known but timestamp is newer


Data to be added in csv. Example for csv
<pre>
timestamp,identifier,lat,lon,cog,sog,alt
1678646309.828,3c61f8,51.765015,5.035004,212,424,40000
1678646308.828,3c5ee2,51.209839,5.413216,,,
1678646308.828,3c5ef2,51.209839,5.413216,,,
</pre>
In AIS no last column 'alt' 

## /adsb/delete/   or /ais/delete/
Delete all data 
