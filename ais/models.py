from django.db import models

"""
timestamp,identifier,lat,lon,cog,sog
1678646308.828,211472620,51.765015,5.035004,212,12
1678646308.828,211883350,51.209839,5.413216,,
"""


class AIS(models.Model):
    # Unix timestamp with milliseconds. Example 1678646308.828
    timestamp = models.FloatField(null=False, help_text="Timestamp of the data")

    # Example 123456789
    identifier = models.CharField(
        max_length=9,
        unique=True,
        null=False,
        default="01ABCD",
        help_text="MMSI of the vessel",
    )

    # Example 51.765015
    lat = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=False,
        default=0,
        help_text="Vessel latitude",
    )

    # Example 5.035004
    lon = models.DecimalField(
        max_digits=9,
        decimal_places=6,
        null=False,
        default=0,
        help_text="Vessel longitude",
    )

    # Example 212
    cog = models.CharField(
        max_length=6, default="", help_text="Vessel course over ground"
    )

    # Example 424
    sog = models.CharField(
        max_length=6, default="", help_text="Vessel ground speed (in nautical miles)"
    )
