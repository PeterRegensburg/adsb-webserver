from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from .models import AIS
from .serializers import AIS_Serializer_to_csv, AIS_Serializer
import csv
from datetime import datetime

from rest_framework_csv import renderers as r
from django.http import HttpResponse
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.response import Response


@api_view(["GET"])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def get(request):
    items = AIS.objects.all()
    serializer = AIS_Serializer(items, many=True)
    return Response(serializer.data)


@api_view(["GET"])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def get_csv(request):
    items = AIS.objects.all()
    try:
        serializer_csv = AIS_Serializer_to_csv(items)
    except:
        return Response("No Data.")

    new = 0
    if new:
        return Response(serializer_csv, content_type="text/csv")
    else:
        response = HttpResponse(serializer_csv, content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="data.csv"'
        return response


@api_view(["POST"])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def update_or_create_csv(request):
    try:
        csv_data = request.body.decode("utf-8")
        csv_data = csv_data.replace("\r\n", "\n")
        data = []

        csv_reader = csv.reader(csv_data.splitlines())

        for row in csv_reader:
            data.append(row)

        field_mapping = {field: index for index, field in enumerate(data[0])}

        print(field_mapping)
        data_list = []

        for row in data[1:]:
            row_dict = {field: row[field_mapping[field]] for field in field_mapping}
            data_list.append(row_dict)

    except Exception as e:
        return Response({"message": f"An error occurred: {str(e)}"}, status=500)

    updated = 0
    created = 0
    ignored = 0

    for data in data_list:
        timestamp = data.get("timestamp")
        identifier = data.get("identifier")

        lat = data.get("lat")
        lon = data.get("lon")
        cog = data.get("cog", "0")
        sog = data.get("sog", "0")

        if cog == "":
            cog = "0"
        if sog == "":
            sog = "0"

        if identifier is not None and timestamp is not None:
            try:
                db_object = AIS.objects.get(identifier=identifier)

                try:
                    ts_float = float(timestamp)
                    try:
                        ts1 = datetime.utcfromtimestamp(ts_float)

                        ts2 = datetime.utcfromtimestamp(db_object.timestamp)

                    except Exception as e:
                        return Response(
                            {"ERRORmessage": f"Conversion timestamp:  {e}"},
                            status=500,
                        )

                    if ts1 > ts2:
                        db_object.timestamp = timestamp
                        db_object.lat = lat
                        db_object.lon = lon
                        db_object.cog = cog
                        db_object.sog = sog
                        db_object.save()
                        updated += 1
                    else:
                        ignored += 1

                except Exception as e:
                    return Response(
                        {"ERRORmessage": f"AIS ERRORR. {e}"},
                        status=500,
                    )

            except AIS.DoesNotExist:
                new_entry = AIS(
                    identifier=identifier,
                    timestamp=timestamp,
                    lat=lat,
                    lon=lon,
                    cog=cog,
                    sog=sog,
                )
                try:
                    new_entry.save()

                except Exception as e:
                    return Response(
                        {
                            "ERRORmessage": f"An error occurred saving new_entry: {str(e)}"
                        },
                        status=500,
                    )

                created += 1

    return Response(
        {
            "message": f"AIS entries updated/{updated } created/{created} ignored/{ignored}."
        },
        status=200,
    )


@api_view(["GET"])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def delete(request):
    id_search = request.query_params.get("identifier", None)
    if id_search == None:
        try:
            AIS.objects.all().delete()
            return Response("All entries have been deleted.")
        except Exception as e:
            return Response(f"An error occurred: {str(e)}")

    if id_search is not None:
        try:
            # identifier = int(identifier)  # Convert mmsi to an integer if needed
            db_object = AIS.objects.filter(identifier=id_search).first()

            if db_object:
                db_object.delete()
                return Response({"message": "Row deleted successfully"})
            else:
                return Response(
                    {"message": f"Identifier {id_search} not found"},
                    status=status.HTTP_404_NOT_FOUND,
                )
        except ValueError:
            return Response(
                {"message": "Invalid identifier parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )
    else:
        return Response(
            {"message": "identifier parameter is missing"},
            status=status.HTTP_400_BAD_REQUEST,
        )
