# https://youtu.be/cJveiktaOSQ?feature=shared
from rest_framework import serializers
from .models import AIS
import csv
from io import StringIO


class AIS_Serializer(serializers.ModelSerializer):
    class Meta:
        model = AIS
        fields = "__all__"


def AIS_Serializer_to_csv(request):
    data = AIS.objects.all()
    serializer = AIS_Serializer(data, many=True)

    output = StringIO(newline="")
    csv_writer = csv.writer(output)

    csv_writer.writerow(serializer.data[0].keys())  # Write header row

    for item in serializer.data:
        csv_writer.writerow(item.values())  # Write data rows

    csv_string = output.getvalue()
    output.close()

    return csv_string
