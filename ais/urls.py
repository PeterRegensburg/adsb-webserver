from django.urls import path
from ais import views


urlpatterns = [
    path("get/", views.get),
    path("add/", views.update_or_create_csv),
    path("delete/", views.delete),
]
